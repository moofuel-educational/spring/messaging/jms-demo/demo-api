package com.moofuel.demo.messaging.demoapi.messaging;

import com.moofuel.demo.messaging.demoapi.dto.ClaimReportDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

/**
 * @author dimon
 * @since 09.04.18
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TemplateProcessorClient {

    private final JmsMessagingTemplate jmsMessagingTemplate;

    public void processTemplate(ClaimReportDto claimReportDto) {
        this.send(claimReportDto);
        log.info("Message was sent to the queue");
    }

    private void send(ClaimReportDto msg) {
        this.jmsMessagingTemplate.convertAndSend("process-reports", msg);
    }
}