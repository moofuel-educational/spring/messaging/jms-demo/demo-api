package com.moofuel.demo.messaging.demoapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dimon
 * @since 09.04.18
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClaimReportDto {
    private Claim claim;
    private String futureDocId;
    private String reportId;
}
