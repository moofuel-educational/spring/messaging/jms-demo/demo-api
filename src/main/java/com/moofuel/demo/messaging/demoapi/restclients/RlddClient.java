package com.moofuel.demo.messaging.demoapi.restclients;

import com.moofuel.demo.messaging.demoapi.dto.Claim;
import com.moofuel.demo.messaging.demoapi.properties.RlddProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author dimon
 * @since 09.04.18
 */
@Service
@RequiredArgsConstructor
@EnableConfigurationProperties(value = RlddProperties.class)
public class RlddClient {

    private final RestTemplate restTemplate;
    private final RlddProperties rlddProperties;

    public Claim findClaimById(String claimId) {
        return restTemplate.getForEntity(rlddProperties.getUrl()+ "/" + claimId,
                Claim.class).getBody();
    }
}