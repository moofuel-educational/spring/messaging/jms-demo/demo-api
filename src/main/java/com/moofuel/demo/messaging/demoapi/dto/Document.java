package com.moofuel.demo.messaging.demoapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dimon
 * @since 09.04.18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Document {

    private String id;
    private String name;
}
