package com.moofuel.demo.messaging.demoapi.service;

import com.moofuel.demo.messaging.demoapi.dto.Claim;
import com.moofuel.demo.messaging.demoapi.dto.ClaimReportDto;
import com.moofuel.demo.messaging.demoapi.dto.Document;
import com.moofuel.demo.messaging.demoapi.messaging.TemplateProcessorClient;
import com.moofuel.demo.messaging.demoapi.restclients.RlddClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author dimon
 * @since 09.04.18
 */
@Service
@RequiredArgsConstructor
public class ClaimService {

    private final TemplateProcessorClient templateProcessorClient;
    private final RlddClient rlddClient;

    public Collection<UUID> generateAllDocsForClaim(String claimId) {
        final Claim claim = this.rlddClient.findClaimById(claimId);
        return claim.getDocuments()
                .stream()
                .map(doc -> sendForProcessing(claim, doc))
                .collect(Collectors.toList());
    }

    private UUID sendForProcessing(Claim claim, Document doc) {
        final UUID futureDocId = UUID.randomUUID();
        templateProcessorClient.processTemplate(
                new ClaimReportDto(claim, futureDocId.toString(), doc.getId()));
        return futureDocId;
    }
}
