package com.moofuel.demo.messaging.demoapi.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author dimon
 * @since 09.04.18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "rldd")
public class RlddProperties {

    private String url;
}