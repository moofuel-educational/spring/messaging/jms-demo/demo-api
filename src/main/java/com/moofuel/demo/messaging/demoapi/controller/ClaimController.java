package com.moofuel.demo.messaging.demoapi.controller;

import com.moofuel.demo.messaging.demoapi.service.ClaimService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.UUID;

/**
 * @author dimon
 * @since 09.04.18
 */
@RestController
@RequestMapping("/claims")
@RequiredArgsConstructor
public class ClaimController {

    private final ClaimService claimService;

    @PostMapping("/{claimId}")
    public Collection<UUID> generate(@PathVariable String claimId) {
        return this.claimService.generateAllDocsForClaim(claimId);
    }
}
