package com.moofuel.demo.messaging.demoapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * @author dimon
 * @since 09.04.18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Claim {

    private Collection<Document> documents;
}
